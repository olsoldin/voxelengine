#pragma once

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <gl\GL.h>
#include <sstream>
#include "GL Files\include\GL\GLAux.h"


template<typename T, unsigned S>
inline unsigned arraysize(const T(&v)[S]){ return S; }

class Utils{
public:
	static GLfloat Max(GLfloat a, GLfloat b, GLfloat c);
	static GLfloat Min(GLfloat a, GLfloat b, GLfloat c);

	static GLfloat Clamp(GLfloat f, GLfloat lower, GLfloat Upper);
	static GLfloat ClampColour(GLfloat f);

	static GLvoid DebugMsgBox(const char* message, ...);

	static GLvoid readstr(FILE* f, char* string);

	static AUX_RGBImageRec *LoadBMP(char *Filename);
};

