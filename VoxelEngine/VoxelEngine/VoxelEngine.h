#pragma once

#define _USE_MATH_DEFINES

#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "GL Files\include\GL\GLAux.h"
#include "Utils.h"
#include "Colour.h"
#include "Vector.h"


//These are free to change
#define WINDOW_TITLE				"Ollie's OpenGL Framework"
#define WINDOW_WIDTH				1920
#define WINDOW_HEIGHT				1080

// These shouldn't be touched
#define WINDOW_COLOUR_BIT_DEPTH		32
#define WINDOW_CLASS_NAME			"OpenGL"
#define SHIFT_MODIFIER				(keys[VK_SHIFT] ? 10.0f : 1.0f)
#define FONT_NUM_CHARS				96

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Decleration for WndProc so CreateGLWindow() can call it


class Engine{
public:
	// Variables
	bool fullscreen = FALSE;

	bool keys[256];			// Array used for the keyboard routine
	bool active = TRUE;		// Window active flag

	// Methods
	BOOL CreateGLWindow(char* title, int width, int height, int bits, bool fullscreenFlag);
	GLvoid KillGLWindow(GLvoid);
	GLvoid ReSizeGLScene(GLsizei width, GLsizei height);

	BOOL RunStep(GLvoid);

	GLvoid glPrint(const char* fmt, ...);
private:
	// Variables
	HINSTANCE hInstance;		// Holds the instance of the application
	HGLRC hRC = NULL;			// Permanent rendering context
	HDC hDC = NULL;				// Private GDI device context
	HWND hWnd = NULL;			// Holds our window handle

	GLuint texture[1];			// Storage for one texture

	GLuint base;				// Base display list for the font set
	GLfloat cnt1;				// 1st counter for movement and colour
	GLfloat cnt2;				// 2nd counter for movement and colour
	
	// Methods
	GLuint InitGL(GLvoid);
	GLuint LoadGLTextures(GLvoid);
	GLuint DrawGLScene(GLvoid);
	GLvoid SwapBuffers(GLvoid);
	GLvoid BuildFont(GLvoid);
	GLvoid KillFont(GLvoid);

	GLvoid glTranslatef(Vector3f vec);
	GLvoid glVertex3f(Vector3f vec);
	GLvoid glNormal3f(Vector3f vec);
	GLvoid glRotatef(GLfloat angle, Vector3f vec);

	// TODO: rename these to glColourRGB, HSV etc.
	GLvoid glColor3f(Colour c);
	GLvoid glColor4f(Colour c);
	GLvoid glClearColor(Colour c);
};