#include "Colour.h"

Colour Colour::WHITE		= Colour(RGB{ 1.0f, 1.0f, 1.0f });
Colour Colour::GREY			= Colour(RGB{ 0.5f, 0.5f, 0.5f });
Colour Colour::BLACK		= Colour(RGB{ 0.0f, 0.0f, 0.0f });

Colour Colour::RED			= Colour(RGB{ 1.0f, 0.0f, 0.0f });
Colour Colour::GREEN		= Colour(RGB{ 0.0f, 1.0f, 0.0f });
Colour Colour::BLUE			= Colour(RGB{ 0.0f, 0.0f, 1.0f });
Colour Colour::YELLOW		= Colour(RGB{ 1.0f, 1.0f, 0.0f });
Colour Colour::MAGENTA		= Colour(RGB{ 1.0f, 0.0f, 1.0f });
Colour Colour::CYAN			= Colour(RGB{ 0.0f, 1.0f, 1.0f });
Colour Colour::ORANGE		= Colour(RGB{ 1.0f, 0.5f, 0.0f });

Colour Colour::DARK_RED		= Colour(RGB{ 0.5f, 0.0f, 0.0f });
Colour Colour::DARK_GREEN	= Colour(RGB{ 0.0f, 0.5f, 0.0f });
Colour Colour::DARK_BLUE	= Colour(RGB{ 0.0f, 0.0f, 0.5f });
Colour Colour::DARK_YELLOW	= Colour(RGB{ 0.5f, 0.5f, 0.0f });
Colour Colour::DARK_MAGENTA = Colour(RGB{ 0.5f, 0.0f, 0.5f });
Colour Colour::DARK_CYAN	= Colour(RGB{ 0.0f, 0.5f, 0.5f });
Colour Colour::DARK_ORANGE	= Colour(RGB{ 0.5f, 0.25f,0.0f });


Colour::Colour(RGBA rgba){
	this->rgba = rgba;
	this->hsva = toHSVA(rgba);
}

Colour::Colour(HSVA hsva){
	this->rgba = toRGBA(hsva);
	this->hsva = hsva;
}



RGBA * Colour::toRGBA(){
	return &rgba;
}

RGBA const * Colour::toRGBA() const{
	return &rgba;
}

RGBA Colour::toRGBA(HSVA hsva) const{
	return toRGBA(hsva.h, hsva.s, hsva.v, hsva.a);
}

RGBA Colour::toRGBA(GLfloat h, GLfloat s, GLfloat v, GLfloat a) const{
	GLfloat hh, p, q, t, ff;
	long i;

	if(s == 0.0f){
		return{ v, v, v, a };
	}

	hh = h;
	if(hh >= 360.0f){
		hh = 0.0f;
	}
	hh /= 60.0f;
	i = (long)hh;
	ff = hh - i;

	p = v * (1.0f - s);
	q = v * (1.0f - (s * ff));
	t = v * (1.0f - (s * (1.0f - ff)));

	switch(i){
		case 0:
			return{ v, t, p, a };
		case 1:
			return{ q, v, p, a };
		case 2:
			return{ p, v, t, a };
		case 3:
			return{ p, q, v, a };
		case 4:
			return{ t, p, v, a };
		case 5:
		default:
			return{ v, p, q, a };
	}
}



HSVA * Colour::toHSVA(){
	return &hsva;
}

HSVA const * Colour::toHSVA() const{
	return &hsva;
}

HSVA Colour::toHSVA(RGBA rgba) const{	
	return toHSVA(rgba.r, rgba.g, rgba.b, rgba.a);
}

HSVA Colour::toHSVA(GLfloat r, GLfloat g, GLfloat b, GLfloat a) const{
	HSVA hsva;
	GLfloat cMin;
	GLfloat cMax;
	GLfloat delta;

	cMin = Utils::Min(r, g, b);
	cMax = Utils::Max(r, g, b);

	hsva.a = a;
	hsva.v = cMax;
	delta = cMax - cMin;

	if(delta < 0.00001f){
		hsva.h = 0;
		hsva.s = 0;
		return hsva;
	}
	// If max == 0, this divide would cause a crash.
	if(cMax > 0.0f){
		hsva.s = delta / cMax;
	} else{
		// If max is 0, then r = g = b = 0
		// so s = 0 and h is undefined
		hsva.s = 0.0f;
		hsva.h = 0.0f;
		return hsva;
	}

	if(r == cMax){
		// Between yellow and magenta
		hsva.h = (g - b) / delta;
	} else if(g == cMax){
		// Between cyan and yellow
		hsva.h = 2.0f + (b - r) / delta;
	} else{
		// Between magenta and cyan
		hsva.h = 4.0f + (r - g) / delta;
	}

	// Degrees
	hsva.h *= 60.0f;

	if(hsva.h < 0.0f){
		hsva.h += 360.0f;
	}

	return hsva;
}