F11			Toggle fullscreen
Alt+Enter	Toggle fullscreen
ESC			Quit program
F 			Loop through filter modes
L 			Toggle lighting
B 			Toggle blending
PG_UP		Move into the screen
PG_DOWN		Move away from the screen

SHIFT		Makes movement faster while it is held down