#pragma once

#include "Vector.h"
#include "Utils.h"




#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "GL Files\include\GL\GLAux.h"
#include "Utils.h"
#include "Colour.h"
#include "Vector.h"


#include <sstream>





typedef struct{
	GLfloat r;	// 0-1
	GLfloat g;	// 0-1
	GLfloat b;	// 0-1
} RGB;

typedef struct{
	GLfloat h;	// 0-360
	GLfloat s;	// 0-1
	GLfloat v;	// 0-1
} HSV;

typedef struct{
	GLfloat r;	// 0-1
	GLfloat g;	// 0-1
	GLfloat b;	// 0-1
	GLfloat a;	// 0-1
} RGBA;

typedef struct{
	GLfloat h;	// 0-360
	GLfloat s;	// 0-1
	GLfloat v;	// 0-1
	GLfloat a;	// 0-1
} HSVA;

class Colour{
public:
	static Colour WHITE;
	static Colour GREY;
	static Colour BLACK;

	static Colour RED;
	static Colour GREEN;
	static Colour BLUE;
	static Colour YELLOW;
	static Colour MAGENTA;
	static Colour CYAN;
	static Colour ORANGE;

	static Colour DARK_RED;
	static Colour DARK_GREEN;
	static Colour DARK_BLUE;
	static Colour DARK_YELLOW;
	static Colour DARK_MAGENTA;
	static Colour DARK_CYAN;
	static Colour DARK_ORANGE;


	Colour()		: Colour(RGBA{  0.0f,  0.0f,  0.0f, 1.0f }){ }
	Colour(RGB rgb) : Colour(RGBA{ rgb.r, rgb.g, rgb.b, 1.0f }){ }
	Colour(HSV hsv) : Colour(HSVA{ hsv.h, hsv.s, hsv.v, 1.0f }){ }
	Colour(RGBA rgba);
	Colour(HSVA hsva);

	RGBA * toRGBA();
	RGBA const * toRGBA() const;
	RGBA toRGBA(HSVA hsv) const;
	RGBA toRGBA(GLfloat h, GLfloat s, GLfloat v, GLfloat a = 1.0f) const;

	HSVA * toHSVA();
	HSVA const * toHSVA() const;
	HSVA toHSVA(RGBA rgba) const;
	HSVA toHSVA(GLfloat r, GLfloat g, GLfloat b, GLfloat a = 1.0f) const;
private:
	RGBA rgba;
	HSVA hsva;
};

