#include "Utils.h"



GLfloat Utils::Max(GLfloat a, GLfloat b, GLfloat c){
	GLfloat d = max(a, b);
	return max(c, d);
}

GLfloat Utils::Min(GLfloat a, GLfloat b, GLfloat c){
	GLfloat d = min(a, b);
	return min(c, d);
}


GLfloat Utils::Clamp(GLfloat f, GLfloat lower, GLfloat upper){
	if(f > upper){
		f = upper;
	} else if(f < lower){
		f = lower;
	}
	return f;
}

GLfloat Utils::ClampColour(GLfloat f){
	return Clamp(f, 0.0f, 1.0f);
}

GLvoid Utils::DebugMsgBox(const char* msg, ...){
	if(FALSE){
		return;
	}
	// Holds our string
	char text[256];

	// Pointer to list of arguments
	va_list ap;

	// If there's no text
	if(msg == NULL){
		return;
	}

	// Parses the string for variables
	va_start(ap, msg);
	{
		vsprintf_s(text, msg, ap);
	}
	va_end(ap);

	std::wstringstream wss(L"");
	wss << text;
	MessageBoxW(NULL, wss.str().c_str(), NULL, MB_OK);
}

GLvoid Utils::readstr(FILE * f, char * string){
	do{
		fgets(string, 255, f);
	} while((string[0] == '/') || (string[0] == '\n'));
}

AUX_RGBImageRec *Utils::LoadBMP(char *Filename){
	FILE *File = NULL;

	// Make sure a filename was given
	if(!Filename){
		return NULL;
	}

	// Try to open the file in "Read" mode
	fopen_s(&File, Filename, "r");

	// Does the file exist?
	if(File){
		// Make sure we close the handle
		fclose(File);
		// Load the bitmap and return a pointer
		return auxDIBImageLoad(Filename);
	}

	// If the load failed, return NULL
	return NULL;
}