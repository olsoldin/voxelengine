#include "VoxelEngine.h"


/**
--------------------------- Public ---------------------------
*/

/**
Lets create a new OpenGL window, using the title given, at the given width and height
and if the fullscreenFlag is set, it will be fullscreen
*/
BOOL Engine::CreateGLWindow(char* title, int width, int height, int bits, bool fullscreenFlag){
	GLuint PixelFormat;	// Holds the pixel format windows gives us
	WNDCLASS wc;		// Window class structure
	DWORD dwExStyle;	// Window extended style
	DWORD dwStyle;		// Window style

	// Create a rectangle the size of the window
	RECT WindowRect;
	WindowRect.left = (long)0;
	WindowRect.right = (long)width;
	WindowRect.top = (long)0;
	WindowRect.bottom = (long)height;

	// Set the global fullscreen flag
	fullscreen = fullscreenFlag;

	// Grab an instance for our window, and set the required parameters
	hInstance = GetModuleHandle(NULL);					// Grab an instance for our window
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;		// Redraw on move, and own DC for window
	wc.lpfnWndProc = (WNDPROC)WndProc;					// WndProc handles messages
	wc.cbClsExtra = 0;									// No extra window data
	wc.cbWndExtra = 0;									// No extra window data
	wc.hInstance = hInstance;							// Set the instance
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);				// Load the default icon
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);			// Load the arrow pointer
	wc.hbrBackground = NULL;							// No background required for GL
	wc.lpszMenuName = NULL;								// We don't want a menu
	wc.lpszClassName = WINDOW_CLASS_NAME;				// Set the class name

														// Are we able to register the window class?
	if(!RegisterClass(&wc)){
		MessageBox(NULL, "Register class failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	if(fullscreen){
		// Switching to fullscreen mode should use the same width and height
		// as the windowed mode, it should be set before the window is created
		DEVMODE dmScreenSettings;									// Device mode
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));		// Makes sure the memory is cleared
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);			// Size of the devmode structure
		dmScreenSettings.dmPelsWidth = width;						// Selected screen width
		dmScreenSettings.dmPelsHeight = height;						// Selected screen height
		dmScreenSettings.dmBitsPerPel = bits;						// Selected bits per pixel
																	// Now we need to say what fields are set
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Can we set the selected fullscreen mode?
		if(ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL){
			// If not successful, then quit or run in windowed mode
			if(MessageBox(NULL, "The requested fullscreen mode is not supported by your video card. Use windowed mode instead?", "NeHe GL", MB_YESNO | MB_ICONEXCLAMATION) == IDYES){
				fullscreen = FALSE;
			} else{
				MessageBox(NULL, "Program will now close.", "ERROR", MB_OK | MB_ICONSTOP);
				return FALSE;
			}
		}
	}

	// Are we still in fullscreen mode?
	if(fullscreen){
		dwExStyle = WS_EX_APPWINDOW;
		dwStyle = WS_POPUP;
		ShowCursor(FALSE);
	} else{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = WS_OVERLAPPEDWINDOW;
	}

	// Adjust window to true requested size
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);

	// TODO: make this look nicer
	if(!(hWnd = CreateWindowEx(dwExStyle,								// Extended style for the window
							   WINDOW_CLASS_NAME,						// Class name
							   title,									// Window title
							   dwStyle |								// Defined window style
							   WS_CLIPSIBLINGS |						// Required window style
							   WS_CLIPCHILDREN,							// Required window style
							   0, 0,									// Window position
							   WindowRect.right - WindowRect.left,		// Calculate window width
							   WindowRect.bottom - WindowRect.top,		// Calculate window height
							   NULL,									// No parent window
							   NULL,									// No menu
							   hInstance,								// Instance
							   NULL))){									// Don't pass anything to WM_CREATE
		KillGLWindow();
		MessageBox(NULL, "Window creation failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	// pfd Tells Windows How We Want Things To Be
	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),									// Size of this pixel format descriptor
		1,																// Version number
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,		// Format must support window, opengl and double buffering
		PFD_TYPE_RGBA,													// Request an RGBA format
		bits,															// Select our colour depth
		0, 0, 0, 0, 0, 0,												// Colour bits ignored
		0,																// No alpha buffer
		0,																// Shift bit ignored
		0,																// No accumulation buffer
		0, 0, 0, 0,														// Accumulation bits ignored
		16,																// 16bit z-buffer (depth buffer)
		0,																// No stencil buffer
		0,																// No auxiliary buffer
		PFD_MAIN_PLANE,													// Main drawing layer
		0,																// Reserved
		0, 0, 0															// Layer masks ignored
	};

	// Did we get a device context?
	if(!(hDC = GetDC(hWnd))){
		KillGLWindow();
		MessageBox(NULL, "GL device context failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Did windows find a matching pixel format?
	if(!(PixelFormat = ChoosePixelFormat(hDC, &pfd))){
		KillGLWindow();
		MessageBox(NULL, "Retrieving pixel format failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Can we set the pixel format?
	if(!SetPixelFormat(hDC, PixelFormat, &pfd)){
		KillGLWindow();
		MessageBox(NULL, "Setting  pixel format failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Can we get a rendering context?
	if(!(hRC = wglCreateContext(hDC))){
		KillGLWindow();
		MessageBox(NULL, "Creating a GL rendering context failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Can we activate the rendering context?
	if(!wglMakeCurrent(hDC, hRC)){
		KillGLWindow();
		MessageBox(NULL, "Activate rendering context failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	ShowWindow(hWnd, SW_SHOW);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);
	ReSizeGLScene(width, height);

	// Can we initialise the newly created GL window?
	if(!InitGL()){
		KillGLWindow();
		MessageBox(NULL, "Initialisation failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	// Everything went OK
	return TRUE;
}

/**
Properly kill the window
*/
GLvoid Engine::KillGLWindow(GLvoid){
	if(fullscreen){
		// Switch back to desktop and show the cursor
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(TRUE);
	}

	// Do we have a rendering context?
	if(hRC){
		// Are we able to release the DC and RC contexts?
		if(!wglMakeCurrent(NULL, NULL)){
			MessageBox(NULL, "Release of DC and RC failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		// Are we able to delete the RC?
		if(!wglDeleteContext(hRC)){
			MessageBox(NULL, "Release rendering context failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		hRC = NULL;
	}

	// Are we able to release the DC?
	if(hDC && !ReleaseDC(hWnd, hDC)){
		MessageBox(NULL, "Release device context failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hDC = NULL;
	}

	// Are we able to destroy the window?
	if(hWnd && !DestroyWindow(hWnd)){
		MessageBox(NULL, "Release window failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hWnd = NULL;
	}

	// Are we able to unregister the class?
	if(!UnregisterClass(WINDOW_CLASS_NAME, hInstance)){
		MessageBox(NULL, "Unregister class failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;
	}

	// Destroy the font
	KillFont();
}

/**
Resize the scene to the given width and height
*/
GLvoid Engine::ReSizeGLScene(GLsizei width, GLsizei height){
	// Prevent a divide by 0 error
	if(height == 0){
		height = 1;
	}
	// Reset the current viewport
	glViewport(0, 0, width, height);

	// Select the projection matrix and reset it
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Calculate the aspect ratio and set the perspective based on it
	GLfloat aspectRatio = (GLfloat)width / (GLfloat)height;
	gluPerspective(45.0f, aspectRatio, 0.1f, 100.0f);

	// Select the ModelView matrix and reset it
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/**
Main loop
Return TRUE if the program is terminated
TODO: move all the key handling stuff to a keys class maybe?
*/
BOOL Engine::RunStep(GLvoid){
	// Draw the scene. Watch for ESC key and QUIT messages from DrawGLScene()
	if(active){
		if(keys[VK_ESCAPE]){
			return TRUE;
		} else{
			// Draw the scene to a hidden buffer, then swap it in so we get flicker free animation
			// (double buffering)
			DrawGLScene();
			SwapBuffers();

			// Toggle fullscreen with f11
			if(keys[VK_F11]){
				keys[VK_F11] = FALSE;
				KillGLWindow();
				fullscreen = !fullscreen;
				// Recreate out openGL window
				if(!CreateGLWindow(WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_COLOUR_BIT_DEPTH, fullscreen)){
					return TRUE;
				}
			}
		}
	}

	// Loop again
	return FALSE;
}

GLvoid Engine::glPrint(const char* fmt, ...){
	// Holds our string
	char text[256];

	// Pointer to list of arguments
	va_list ap;

	// If there's no text
	if(fmt == NULL){
		return;
	}

	// Parses the string for variables
	va_start(ap, fmt);
	{
		// And converts symbols to actual numbers
		vsprintf_s(text, fmt, ap);
	} // Results are stored in text
	va_end(ap);

	// Prevent glListBase from affectng any other display lists
	glPushAttrib(GL_LIST_BIT);

	// Because we ignored the first 32 characters of the font,
	// we need to offset everything by 32 so the characters
	// line up with their display list.
	glListBase(base - 32);

	// Draws the display list text
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);

	// Pops the display list bits
	glPopAttrib();
}

/**
--------------------------- Private ---------------------------
*/

/**
Initialise everything we need for an OpenGL window
*/
GLuint Engine::InitGL(GLvoid){
	// End the program early if the textures aren't loaded
	/*if(!LoadGLTextures()){
		return FALSE;
	}*/

	// Enable smooth shading
	glShadeModel(GL_SMOOTH);

	// Black Background
	glClearColor(Colour::BLACK);

	// Depth buffer setup
	glClearDepth(1.0f);

	// Enable depth testing
	glEnable(GL_DEPTH_TEST);

	// The type of depth test to do
	glDepthFunc(GL_LEQUAL);

	// Nice perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Build the font
	BuildFont();

	// Everything went OK
	return TRUE;
}

GLuint Engine::LoadGLTextures(GLvoid){
	int Status = FALSE;

	// Create storage space for the texture
	AUX_RGBImageRec *TextureImage[1];

	// Make sure the memory is empty (just in case)
	memset(TextureImage, 0, sizeof(void*) * 1);

	// Load the bitmap, check for errors, if bitmap isn't found then quit
	if(TextureImage[0] = Utils::LoadBMP("Data/Textures/Cube.bmp")){
		Status = TRUE;

		// Create the texture
		::glGenTextures(arraysize(texture) - 1, &texture[0]);

		// Typical texture generation using data from the bitmap
		::glBindTexture(GL_TEXTURE_2D, texture[0]);

		// Generate the texture
		::glTexImage2D(GL_TEXTURE_2D,				// Texture type
					   0,							// Level of detail
					   3,							// Number of data components (RGB)
					   TextureImage[0]->sizeX,		// Width
					   TextureImage[0]->sizeY,		// Height
					   0,							// Border
					   GL_RGB,						// Data Format (Red, then Green, then Blue)
					   GL_UNSIGNED_BYTE,			// Data type
					   TextureImage[0]->data);		// Data location

		// What filtering shall we use for the texture when it is
		// larger and smaller than the actual size
		// GL_LINEAR	- makes it look smooth, but uses GPU power
		// GL_NEAREST	- makes it look blocky, but doesnt use GPU power
		::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	} else{
		MessageBox(NULL, "Error loading bitmap", "ERROR", MB_OK | MB_ICONSTOP);
	}

	// Clear any memory we used
	if(TextureImage[0]){
		if(TextureImage[0]->data){
			free(TextureImage[0]->data);
		}
		free(TextureImage[0]);
	}

	return Status;
}

GLvoid Engine::BuildFont(GLvoid){
	HFONT font;				// Windows font ID
	HFONT oldFont;			// Used for good house keeping

	base = glGenLists(FONT_NUM_CHARS);	// Storage for FONT_NUM_CHARS(96) characters

	font = CreateFont(-24,							// Height of font
					  0,							// Width of font (0=windows default)
					  0,							// Angle of escapement (rotate the font)
					  0,							// Orientation angle
					  FW_BOLD,						// Font weight
					  FALSE,						// Italic?
					  FALSE,						// Underline?
					  FALSE,						// Strikeout?
					  ANSI_CHARSET,					// Character set identifier
					  OUT_TT_ONLY_PRECIS,			// Output precision
					  CLIP_DEFAULT_PRECIS,			// Clipping precision
					  ANTIALIASED_QUALITY,			// Output quality (PROOF, DRAFT, NONANTIALIASED, DEFAULT, ANTIALIASED)
					  FF_DONTCARE |					// Family (DECORATIVE, MODERN, ROMAN, SCRIPT, SWISS, DONTCARE)
					  DEFAULT_PITCH,				// Pitch (DEFAULT, FIXED, VARIABLE)
					  "Courier New");				// Font name

	// SelectObject returns the font that was previously set
	// when switching to the new font, 
	oldFont = (HFONT)SelectObject(hDC, font);
	
	// Builds FONT_NUM_CHARS(96) characters starting at character 32
	wglUseFontBitmaps(hDC, 32, FONT_NUM_CHARS, base);

	// Selects the font we want
	SelectObject(hDC, oldFont);
	DeleteObject(font);
}

GLvoid Engine::KillFont(GLvoid){
	// Delete all FONT_NUM_CHARS(96) characters
	glDeleteLists(base, FONT_NUM_CHARS);
}

/**
All the drawing goes here
*/
GLuint Engine::DrawGLScene(GLvoid){
	// Clear the screen and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Rotate
	Vector3f vecXmask(1.0f, 0.0f, 0.0f);
	Vector3f vecYmask(0.0f, 1.0f, 0.0f);
	Vector3f vecZmask(0.0f, 0.0f, 1.0f);

	// Reset the view
	glLoadIdentity();

	// Move 1 unit into the screen
	glTranslatef(Vector3f(0.0f, 0.0f, -1.0f));

	// Pulsing colours based on text position
	GLfloat red		= 1.0f * float(cos(cnt1));
	GLfloat green	= 1.0f * float(sin(cnt2));
	GLfloat blue	= 1.0f - 0.5f * float(cos(cnt1 + cnt2));
	Colour c(RGB{ red, green, blue });
	glColor3f(c);

	// TODO: make a vector2f version
	// Position the text on the screen
	glRasterPos2f(-0.45f + 0.05f * float(cos(cnt1)), 0.35f * float(sin(cnt2)));

	// Print GL text to the screen
	glPrint("Active OpenGL text with Ollie - %7.2f", cnt1);

	// Increase the counters
	cnt1 += 0.051f;
	cnt2 += 0.005f;

	// Everything went OK
	return TRUE;
}

GLvoid Engine::SwapBuffers(GLvoid){
	::SwapBuffers(hDC);
}

GLvoid Engine::glTranslatef(Vector3f vec){
	::glTranslatef(vec[0], vec[1], vec[2]);
}

GLvoid Engine::glVertex3f(Vector3f vec){
	::glVertex3f(vec[0], vec[1], vec[2]);
}

GLvoid Engine::glNormal3f(Vector3f vec){
	::glNormal3f(vec[0], vec[1], vec[2]);
}

GLvoid Engine::glRotatef(GLfloat angle, Vector3f vec){
	::glRotatef(angle, vec[0], vec[1], vec[2]);
}


GLvoid Engine::glColor3f(Colour c){
	::glColor3f(c.toRGBA()->r, c.toRGBA()->g, c.toRGBA()->b);
}

GLvoid Engine::glColor4f(Colour c){
	::glColor4f(c.toRGBA()->r, c.toRGBA()->g, c.toRGBA()->b, c.toRGBA()->a);
}

GLvoid Engine::glClearColor(Colour c){
	::glClearColor(c.toRGBA()->r, c.toRGBA()->g, c.toRGBA()->b, c.toRGBA()->a);
}


/**
--------------------------- Global stuff here ---------------------------
*/

Engine engine = Engine();

/**
	This is where all the window messages are dealt with
*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	switch(uMsg){
		// Watch for windows activate message
		case WM_ACTIVATE:
			// Check minimisation state
			engine.active = !HIWORD(wParam);
			return 0;

			// Intercept system commands
		case WM_SYSCOMMAND:
			// Check system calls
			switch(wParam){
				case SC_SCREENSAVE:		// Screensaver trying to start?
				case SC_MONITORPOWER:	// Monitor trying to enter powersave?
					return 0;			// Prevent from happening
			}
			break;

			// Close message
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

			// Key being held down
		case WM_KEYDOWN:
			engine.keys[wParam] = TRUE;
			return 0;

			// Key being released
		case WM_KEYUP:
			engine.keys[wParam] = FALSE;
			return 0;

			// Resize the openGL window
		case WM_SIZE:
			engine.ReSizeGLScene(LOWORD(lParam), HIWORD(lParam)); // LoWord = width, HiWord = height
			return 0;
	}
	// Pass everything else to DefWindowProc
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/**
	Entry point of our application
*/
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
	MSG msg;
	BOOL done = FALSE;

	// Ask the user which screen mode they prefer
	if(MessageBox(NULL, "Would you like to run in fullscreen mode", "Start fullscreen?", MB_YESNO | MB_ICONQUESTION) == IDNO){
		engine.fullscreen = FALSE;
	}

	// Create the openGL window
	if(!engine.CreateGLWindow(WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_COLOUR_BIT_DEPTH, engine.fullscreen)){
		// Quit if the window wasn't created
		return 0;
	}

	while(!done){
		// Is there a message waiting?
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
			// Have we recieved a quit message?
			if(msg.message == WM_QUIT){
				done = TRUE;
			} else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} else{ // There are no messages
			done = engine.RunStep();
		}
	}
	// Shutdown
	engine.KillGLWindow();
	return (msg.wParam);
}